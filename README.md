
Example of how to generate python documentation for CDKTF and the AWS provider for CDKTF.
Note: sphinx-autodoc-example is a simpler sample for reference, not related to this project.

**pre-requisites:**

This was the process to set up this environment:
```bash
cdktf init --template=python --local
```

Edit cdktf.json, change:
```json
  "terraformProviders": [],
```

to:

```json
  "terraformProviders": [
    "hashicorp/aws@~> 3.42"
  ],

```

Then (optional, `pip install cdktf-cdktf-provider-aws` should substitute for the `cdktf get` step, which takes a long time.):

```bash
cdktf get
```
> Generated python constructs in the output directory: imports


```bash
PIPENV_VENV_IN_PROJECT="enabled" pipenv install
```


<!-- 

example of how to add a dependency using pipenv

```bash
pipenv install sphinx
```

>Installing sphinx...  
Adding sphinx to Pipfile's [packages]...  
✔ Installation Succeeded 
-->


```bash
mkdir docs
cd docs
pipenv shell
sphinx-quickstart                                                                                                                                                                  
```

>Welcome to the Sphinx 4.1.1 quickstart utility.<br/><br/>
Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).
<br/><br/>
Selected root path: .
<br/><br/>
You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.
<br/><br/>
&nbsp;> Separate source and build directories (y/n) [n]: y
<br/><br/>
The project name will occur in several places in the built documentation.
<br/><br/>
&nbsp;> Project name: CDKTF AWS
<br/><br/>
&nbsp;> Author name(s): .
<br/><br/>
&nbsp;> Project release []:
<br/><br/>
If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.
<br/><br/>
For a list of supported codes, see
https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
<br/><br/>
&nbsp;> Project language [en]:
<br/><br/>
Creating file /Users/jnorment/proj/cogz/cdktf-demo/source/conf.py.
Creating file /Users/jnorment/proj/cogz/cdktf-demo/source/index.rst.
Creating file /Users/jnorment/proj/cogz/cdktf-demo/Makefile.
Creating file /Users/jnorment/proj/cogz/cdktf-demo/make.bat.
<br/><br/>
Finished: An initial directory structure has been created.
<br/><br/>
You should now populate your master file /Users/jnorment/proj/cogz/cdktf-demo/source/index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
   make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.


Made this change to docs/source/conf.py:
( See note at end )


```python
# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

...

extensions = [
]
```

to:

```python
# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../..'))
sys.setrecursionlimit(1500)

...

extensions = [ 'sphinx.ext.todo', 'sphinx.ext.viewcode', 'sphinx.ext.autodoc' ]
```

Made these changes to docs/source/index.rst:

```rst
Welcome to CDKTF AWS's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```

to:

```rst
.. toctree::
   :maxdepth: 2
   :caption: Contents:


CDKTF init
===================
.. automodule:: imports.aws
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```

Note: Changes were made later to include cdktf documentation. ... I didn't take the time to figure out how to capture both the aws package and the cdktf packages in a single generation statement.
