.. CDKTF AWS documentation master file, created by
   sphinx-quickstart on Thu Jul 15 13:46:22 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CDKTF AWS's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules


..
  CDKTF init
  ===================
.. 
   automodule:: aws
   :members:
.. 
   automodule:: cdktf
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
